// NextJS / React components
import { NextPage } from "next"
import Link from "next/link"
import Head from "next/head"

// Styles
import style from "@styles/Index.module.css"

// FontAwesome logos
import {
	faBook,
	faAddressCard,
	faBriefcase,
	faFileDownload,
	faEnvelope,
} from "@fortawesome/free-solid-svg-icons"
import {
	faGitlab,
	faTwitch,
	faTwitter,
	faLinkedin,
	faYoutube,
	faGithub,
} from "@fortawesome/free-brands-svg-icons"

// Home made Components
import OmnyProject from "@components/project"
import OmnyButton from "@components/button"
import OmnySeparator from "@components/separator"
import { OmnyLink, OmnyNav } from "@components/navui"
import OmnySocialLink from "@components/sociallink"

// Assets
import OBSLogo from "@assets/obs_logo.png"
import TwitchLogo from "@assets/twitch_logo.png"
import JavaLogo from "@assets/java_logo.png"
import SpigotLogo from "@assets/spigot_logo.png"
import OtomnyLogo from "@assets/otomny_logo.png"
import FlutterLogo from "@assets/flutter.png"
import MinecraftLogo from "@assets/minecraft256.png"
import LeTrollVM from "@assets/VM.png"
import Python from "@assets/brand/python-5.svg"
import Competences from "@components/competences"

const Index: NextPage = () => {
	const headerStyle = style.header
	//year, monthIndex, day

	const birthDate = new Date(2000, 7, 22).valueOf()
	const today = Date.now()
	const age = Math.abs(new Date(today - birthDate).getUTCFullYear() - 1970)

	return (
		<>
			<div></div>
			<header className={headerStyle}>
				<OmnyNav>
					<OmnyLink
						href={"#about"}
						icon={faAddressCard}
						name={"Qui suis-je ?"}
						index={1}
						alt={"Aller à l'endroit A propos"}
					/>
					<OmnyLink
						href={"#experiences"}
						icon={faBook}
						name={"Expériences"}
						index={2}
						alt={"Aller à l'endroit Expériences"}
					/>
					<OmnyLink
						href={"#projets"}
						icon={faBriefcase}
						name={"Projets"}
						index={3}
						alt={"Aller à l'endroit Projets"}
					/>
				</OmnyNav>
				<div className={style.headerdesc}>
					<span className={style.iam}>Je suis</span>
					<h1>
						<strong>Fabien CAYRE</strong>
					</h1>
					<p>Etudiant ingénieur en réseaux informatiques</p>
					<p>
						<strong>En recherche de CDI sur Reims</strong>
						<OmnySocialLink
							brandType={faLinkedin}
							alt={"Lien vers mon compte LinkedIn"}
							link={"https://www.linkedin.com/in/fabien-cayre-44332b177/"}
							noColor={true}
						/>
					</p>
				</div>
			</header>
			<OmnySeparator margin={"0px"} />
			<main className={style.main}>
				<article className={style.article + " " + style.about} id='about'>
					<div className={style.articlecontent}>
						<span className={style.iam}>Je suis</span>
						<h1>
							<strong>Fabien CAYRE</strong>
						</h1>
						<h2>
							Etudiant ingénieur à
							<Link href={"https://www.upssitech.eu/"}>
								<a target='_blank' rel='noopener noreferrer'>
									<strong> UPSSITECH</strong>
								</a>
							</Link>{" "}
							en dernière année
						</h2>
						<h3 className={style.recherchestage}>
							Actuellement en recherche de CDI sur Reims
						</h3>
						<p>
							J'ai {age} ans, j'habite actuellement <strong>Toulouse</strong> et
							je suis intéressé par l'intégration, le monitoring et la
							philosophie DevOps . Je suis étudiant dans le département
							ingénieur Système de Télécommunication et Réseaux Informatiques
							(STRI), en dernière année à UPSSITECH.
						</p>
						<p>
							Même si je n'ai pas encore beaucoup d'expériences professionelles,
							j'ai effectué de multiples projets en parallèle de mes études, ce
							qui m'a permis d'acquérir des compétences aussi bien techniques
							que personnelles, comme la communication entre membres d'une
							équipe, ou bien la conception d'application scalable.
						</p>
						<OmnyButton
							text={"Télécharger mon CV"}
							link={"/files/CV_CAYRE_Fabien.pdf"}
							alt={"Télécharger le CV de Fabien CAYRE"}
							outline={true}
							icon={faFileDownload}
						></OmnyButton>
					</div>
				</article>
				<OmnySeparator />
				<article
					className={style.article + " " + style.project}
					id='experiences'
				>
					<div className={style.articlecontent}>
						<h1>Mes expériences</h1>

						<div className={style.projects}>
							<OmnyProject>
								<h3>Erasmus - Åbo Akademi, Finlande</h3>
								<span>Septembre 2022 - Décembre 2022</span>
								<p>Semestre de Master 2 d'Informatique à l'étranger</p>
								<ul>
									<li>
										<strong>Parallel programming</strong>: Programmation
										d'application en C avec l'API MPI et OpenMP.
									</li>
									<li>
										<strong>Advanced Text Algorithm</strong>: Algorithmique
										appliquée sur la recherche de texte, de patternes, etc...
									</li>
									<li>
										<strong>Web Technologies</strong>: Création d'un projet web
										de boutique electronique minimale avec React en front et
										Django en back.
									</li>
									<li>
										<strong>Software Quality</strong>: Apprentissage des bonnes
										pratiques de développement logiciel et d'assurance qualité.
									</li>
									<li>
										<strong>GPU Programming</strong>: Conception d'un code
										utilisant l'architecture CUDA pour la parallélisation de
										calculs. Le but étant de "prouver" la présence de matière
										noire dans l'univers en comparant des simulations avec des
										observations.
									</li>
								</ul>
							</OmnyProject>
							<OmnyProject>
								<h3>Tisséo Voyageur - Stage de 2éme année STRI</h3>
								<span>fin Mars 2022 - fin Aout 2022</span>
								<p>
									Création, migration et containerisation d'application sur une
									plateforme <strong>CI/CD</strong>
								</p>
								<ul>
									<li>
										Conception et création de pipelines <strong>Jenkins</strong>{" "}
										d'intégration continue, du serveur de recette jusqu'au
										serveur de production.
									</li>
									<li>
										Mise en place de passage de qualité de code sur la pipeline
										Jenkins à l'aide d'un serveur <strong>SonarQube</strong>{" "}
										containerisé
									</li>
									<li>
										Montée en version d'applications Python 2.7 vers Python 3.8.
									</li>
									<li>Montée en version d'applications PHP.</li>
									<li>
										Containerisation d'applications Python (Flask + uWSGI) et
										PHP (Drupal) dans des images <strong>Docker</strong> de
										taille minimale.
									</li>
									<li>
										Conception, documentation, containerisation et création
										d'une application Python (Flask + uWSGI) de passerelle entre
										deux services d'identification. Optimisation de celle-ci
										avec un cache <strong>Redis</strong>.
									</li>
								</ul>
							</OmnyProject>
							<OmnyProject>
								<h3>ARSOE de Soual - Stage DUT</h3>
								<span>Avril 2020 - Juin 2020 + CDI Juillet 2020</span>
								<p>
									Rédaction, Modélisation et Conception d'une interface
									permettant de récupérer certains types de statistiques sur un
									élevage bovin en fonction d'un contrat.
								</p>
								<ul>
									<li>
										Rédaction d'un document d'analyse des besoins du clients,
										ainsi que les différentes manières de récupérer les données.
									</li>
									<li>
										Conception d'une interface web permettant de paramétrer le
										document contenant les statistiques
									</li>
									<li>
										Création de la partie backend en utilisant{" "}
										<strong>C#</strong> sous <strong>.NET</strong>, le moteur de
										template <strong>Microsoft Razor</strong>, utilisation de
										JavaScript pour une meilleure expérience de l'utilisateur
									</li>
								</ul>
							</OmnyProject>
						</div>
					</div>
				</article>
				<OmnySeparator />
				<article
					className={style.article + " " + style.project}
					id='projets_decouverte'
				>
					<div className={style.articlecontent}>
						<h1
							style={{
								marginBottom: 0,
							}}
						>
							Mes Compétences
						</h1>
						<h2 className={style.subtitle}>
							Langages et Outils utilisés à travers mes projets, mes expériences
							professionelles et mes travaux universitaires
						</h2>
						<Competences />
					</div>
				</article>
				<OmnySeparator />
				<article className={style.article + " " + style.project} id='projets'>
					<div className={style.articlecontent}>
						<h1>Mes projets</h1>
						<div className={style.projects}>
							<OmnyProject image={SpigotLogo}>
								<h3>Flow</h3>
								<p>API OpenSource pour du développement sous Minecraft</p>
								<p>
									Comme pour tout projet informatique, la factorisation de code
									est importante. Notamment avec Minecraft qui ne propose que
									leur API bas niveau pour les développeurs.
								</p>
								<p>
									Ayant créé une abstraction haut niveau pour faciliter la
									création de fonctionnalités sur mon serveur, j'ai pensé que ce
									serait une bonne idée d'en faire part aux autres développeurs
									de la communauté
								</p>
								<OmnyButton
									link={"https://github.com/Otomny/flow"}
									text={"Lien Github"}
									icon={faGithub}
									alt={
										"Lien vers le github de l'api de développement Minecraft"
									}
									outline={true}
								></OmnyButton>
							</OmnyProject>
							<OmnyProject image={JavaLogo}>
								<h3>Projets Universitaires</h3>
								<p>
									Programmation Concurrente (C, C++, Java), Programmation sur
									GPU (C++, CUDA), Développement Web (JavaScript, PHP)
								</p>
								<p>
									Tous les projets en lien avec la programmation que j'ai
									effectué seul ou en équipe dans le cadre de mes études sont
									répertoriés sur GitLab. Il y a également les projets que j'ai
									réalisé pendant mon <strong>Erasmus</strong> en Finlande.
								</p>
								<OmnyButton
									link={"https://gitlab.com/up6tech"}
									text={"Lien Git"}
									alt={"Lien vers le GitLab vers les projets universitaires"}
									outline={true}
									icon={faGitlab}
								></OmnyButton>
							</OmnyProject>

							<OmnyProject image={OtomnyLogo}>
								<h3>Otomny</h3>
								<p>Serveur Minecraft</p>
								<p>
									J'en parle beaucoup dans tous les autres projets, et le voici.
									Minecraft est un des jeux-video au quel j'ai le plus joué de
									toute ma vie, c'est ce jeu qui m'a donné l'envie de
									programmer. Et si j'en suis là aujourd'hui, c'est en partie
									graçe à lui.
								</p>
								<p>
									Ce serveur représente toutes mes années d'expériences, que ce
									soit en tant que joueur et développeur.
								</p>
								<OmnyButton
									link={"https://otomny.fr"}
									text={"Lien"}
									alt={"Lien vers le site Otomny.fr, mon serveur"}
									outline={true}
								></OmnyButton>
							</OmnyProject>
						</div>
					</div>
				</article>
				<article
					className={style.article + " " + style.project}
					id='projets_decouverte'
				>
					<div className={style.articlecontent}>
						<h2>Mini-Projet découverte</h2>
						<h3>
							Je suis quelqu'un de très curieux par nature, et quand je vois une
							nouvelle technologie, ou une vidéo sur des reproductions de jeu,
							je suis attiré inévitablement par l'envie d'essayer/de reproduire
							cela par moi même.
						</h3>
						<div className={style.projects}>
							<OmnyProject image={FlutterLogo}>
								<h3>MuscleHack</h3>
								<p>
									Application multiplateforme pour la mise en place de session
									de musculation, ainsi que du tracking de l'évolution
								</p>
								<p>
									J'ai commencé à faire régulièrement du sport, et surtout de la
									musculation. Il me fallait un outil me permettant de m'aider à
									m'entrainer pour ne pas perdre du temps.
								</p>
								<p>
									Pour ce faire, j'ai créé une application sous Flutter, un
									framework de google pour la création d'application mobile
									multiplateforme (iOS, Android).
								</p>
								<OmnyButton
									link={"https://github.com/ComminQ/MuscleHack"}
									text={"Lien Github"}
									alt={"Lien vers le Github de l'application musclatax"}
									outline={true}
									icon={faGithub}
								/>
							</OmnyProject>
							<OmnyProject image={Python}>
								<h3>Vaulthon</h3>
								<p>
									Un script python permettant de générer des fichiers chiffrés
									via AES128 avec un mot de passe et de les déchiffrer.
								</p>
								<p>
									Cet outil est destiner a stocker des configurations ou des
									mots de passe de manière sécurisée. Sur des projets de tailles
									moyenne
								</p>
								<OmnyButton
									link={"https://github.com/ComminQ/Vaulthon"}
									text={"Lien Github"}
									alt={"Lien vers le Github du projet"}
									outline={true}
									icon={faGithub}
								/>
							</OmnyProject>
							<OmnyProject image={JavaLogo}>
								<h3>Omny</h3>
								<p>Un framework Java pour la création d'application web</p>
								<p>
									Suite à la création d'un serveur web en C lors de mes études,
									j'ai voulu réitérer l'expérience mais en Java. Et pour
									faciliter la création d'application web, j'ai créé un
									framework. Ce framework utilise la bibliothèque JDK fournis
									par Oracle, et ne nécessite pas de serveur d'application
									(JBoss, Tomcat, etc...). Les concepts utilisés sont les
									suivants:
								</p>
								{/* - Réflection (Injection de dépendance, Annotation,
									etc...) - Gestion de la concurrence (Pool de thread,
									Synchronisation) */}
								<ul>
									<li>
										Réflection (Injection de dépendance, Annotation, etc...)
									</li>
									<li>
										Gestion de la concurrence (Pool de thread, Synchronisation)
									</li>
								</ul>
								<OmnyButton
									link={"https://github.com/ComminQ/Omny"}
									text={"Lien Github"}
									alt={"Lien vers le Github du projet"}
									outline={true}
									icon={faGithub}
								/>
							</OmnyProject>

							<OmnyProject image={LeTrollVM}>
								<h3>LeTrollVM</h3>
								<p>
									Machine Virtuelle Java écrites en Typescript et executée avec
									le runtime BunJS.
								</p>
								<p>
									Je suis abonné au développeur{" "}
									<a
										href='https://www.youtube.com/c/TsodingDaily'
										style={{
											color: "#4a8cfc",
											textDecoration: "underline",
										}}
									>
										Tsoding
									</a>{" "}
									qui fait de temps en temps des projets divers / découverte de
									nouvelles technologies. Et dans{" "}
									<a
										href='https://www.youtube.com/watch?v=anOidUQcv1w'
										style={{
											color: "#4a8cfc",
											textDecoration: "underline",
										}}
									>
										cette vidéo
									</a>{" "}
									il développe une machine virtuelle java minimaliste capable
									d'éxecuter un Hello World.
								</p>
								<p>
									J'ai voulu reproduire cette idée mais avec un autre langage de
									programmation, Typescript. Mais pour éxecuter du code
									Typescript de manière transparente il faut un autre
									environnement d'execution que NodeJS. Et il en existe un,{" "}
									<a
										href='https://bun.sh/'
										style={{
											color: "#4a8cfc",
											textDecoration: "underline",
										}}
									>
										BunJS
									</a>
									, qui est prométteur.
								</p>
								<OmnyButton
									link={"https://github.com/ComminQ/letrollvm"}
									text={"Lien Github"}
									alt={"Lien vers le Github de la JVM LeTrollVM"}
									outline={true}
									icon={faGithub}
								/>
							</OmnyProject>
							{/* <OmnyProject image={OBSLogo}>
								<h3>Outils OBS</h3>
								<p>
									Outil permettant d'automatiser les modification de scène en
									fonction de la page qui est active
								</p>
								<p>
									Je stream souvent ce que je développpe dans mon temps libre,
									mais je ne voulais pas changer les scènes manuellement, ou
									bien je l'oubliai. J'ai donc créé cet outils pour le faire à
									ma place. On dit souvent que{" "}
									<q>les meilleurs développeurs sont les plus paresseux</q>...
								</p>
								<OmnyButton
									link={"https://gitlab.com/projetperso2/obs-auto-transition"}
									text={"Lien Git"}
									alt={"Lien vers le GitLab du projet OBS Auto Transition"}
									outline={true}
									icon={faGitlab}
								></OmnyButton>
							</OmnyProject> */}
						</div>
					</div>
				</article>
			</main>

			<footer className={style.footer}>
				<div className={style.sociallinks}>
					<OmnySocialLink
						brandType={faTwitter}
						alt={"Lien vers mon compte twitter"}
						link={"https://twitter.com/fabiencayre"}
					/>
					<OmnySocialLink
						brandType={faLinkedin}
						alt={"Lien vers mon compte LinkedIn"}
						link={"https://www.linkedin.com/in/fabien-cayre-44332b177/"}
					/>
					<OmnySocialLink
						brandType={faYoutube}
						alt={"Lien vers ma chaîne Youtube"}
						link={
							"https://www.youtube.com/channel/UCmVRXrJ5Ul9EKFLJRSE1hAQ/featured"
						}
					/>
					<OmnySocialLink
						brandType={faEnvelope}
						alt={"Contact de ma boite mail"}
						link={"mailto:contact@fabiencayre.fr"}
					/>
				</div>
				<p>
					Site développé par CAYRE Fabien, sous NextJS. Les sources sont
					disponibles ci-dessous
				</p>
				<OmnyButton
					text={"Lien Git"}
					icon={faGitlab}
					link={"https://gitlab.com/projetperso2/portfolio"}
					outline={true}
					alt={"Lien vers le GitLab de mon portfolio"}
				></OmnyButton>
			</footer>

			{/* <OmnyButton text={"Me contacter"} outline={true}></OmnyButton>
			<OmnyButton text={"Clique moi!"} link={"https://otomny.fr"}></OmnyButton> */}
		</>
	)
}

export default Index
