import style from "@styles/components/omnynav.module.css"
import Image from "next/image"
import Link from "next/link"
import React, { FC, useState, useEffect } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { IconProp } from "@fortawesome/fontawesome-svg-core"

import { UrlObject } from "url"
// Assets
// import Photo from "@assets/photo.jpg"
import Photo from "@assets/photo2.jpg"
import { faArrowUp } from "@fortawesome/free-solid-svg-icons"
import classNames from "classnames"

declare type Url = string | UrlObject

const WIDTH = 200
const HEIGHT = 200
const checkForScrolling = 280

interface OmnyNavProps {}

const OmnyNav: FC<OmnyNavProps> = (props) => {
	const [isToggled, toggle] = useState(false)
	const [scrolling, setScrolling] = useState(false)
	const [scrollTop, setScrollTop] = useState(0)

	const toggleCss = () => {
		toggle(!isToggled)
	}

	useEffect(() => {
		const onScroll = (e: any) => {
			setScrollTop(e.target.documentElement.scrollTop)
			setScrolling(e.target.documentElement.scrollTop > scrollTop)
		}
		window.addEventListener("scroll", onScroll)

		return () => window.removeEventListener("scroll", onScroll)
	}, [scrollTop])

	const navClasses = classNames({
		[`${style.nav}`]: true,
		[`${style.nav_toggle}`]: isToggled,
	})

	if (scrollTop > checkForScrolling && !isToggled) {
		toggle(!isToggled)
	} else if (scrollTop < checkForScrolling && isToggled) {
		toggle(!isToggled)
	}

	return (
		<>
			<div className={navClasses}>
				<div className={style.fillSquare}></div>
				<div
					className={style.image}
					style={{
						width: WIDTH,
						height: HEIGHT,
					}}
				>
					<Image
						alt={"Photo de Fabien CAYRE"}
						src={Photo}
						width={WIDTH}
						height={HEIGHT}
						placeholder={"blur"}
						objectFit='cover'
					/>
				</div>
				<button
					className={style.buttonUpDown}
					onClick={toggleCss}
					title={"Cacher l'image"}
				>
					{<FontAwesomeIcon icon={faArrowUp} />}
				</button>
				<div className={style.childContainer}>{props.children}</div>
			</div>
		</>
	)
}

interface OmnyLinkProps {
	href: Url
	icon: IconProp
	name: String
	index?: number
	alt: string
}

const OmnyLink: FC<OmnyLinkProps> = (props) => {
	const linkId = `navlink-${props.index}`

	return (
		<>
			<Link href={props.href}>
				<a
					className={style.childWrap}
					data-tip={props.name}
					data-for={linkId}
					title={props.alt}
				>
					<FontAwesomeIcon icon={props.icon} />
					{/*isMounted && (
						<ReactTooltip
							id={linkId}
							place='right'
							effect='solid'
							type={"dark"}
							class={style.tooltip}
							offset={{
								top: 100
							}}
						></ReactTooltip>
						)*/}
				</a>
			</Link>
		</>
	)
}

export { OmnyNav, OmnyLink }
