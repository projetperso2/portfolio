import Box from "@mui/material/Box"
import Tab from "@mui/material/Tab"
import Tabs from "@mui/material/Tabs"
import { FC, useState } from "react"
import BackendSkills from "./competences/BackendSkills"
import GraphicsSkills from "./competences/GraphicsSkills"
import IntegrationSkills from "./competences/IntegrationSkills"
import SoftwareSkills from "./competences/SoftwareSkills"
import SystemSkills from "./competences/SystemSkills"
import style from "@styles/components/competences.module.css"
import FrontendSkills from "./competences/FrontendSkills"

function a11yProps(index: number) {
	return {
		id: `vertical-tab-${index}`,
		"aria-controls": `vertical-tabpanel-${index}`,
		sx: {
			fontFamily: "Roboto Mono",
			fontWeight: "bold",
			fontSize: "16px",
			"&.Mui-selected": {
				backgroundColor: "#fff",
				borderTopLeftRadius: "16px",
				borderBottomLeftRadius: "16px",
			},
		},
		className: style.tab,
	}
}

const Competences: FC = (props) => {
	const [value, setValue] = useState(0)

	const handleChange = (event: React.SyntheticEvent, newValue: number) => {
		setValue(newValue)
	}

	return (
		<Box
			sx={{
				flexGrow: 1,
				display: "flex",
				minHeight: 520,
			}}
			className={style.container}
		>
			<Box
				sx={{
					width: "300px",
				}}
			>
				<Tabs
					orientation='vertical'
					value={value}
					onChange={handleChange}
					TabIndicatorProps={{
						style: { display: "none" },
					}}
				>
					{/* <Tab disableRipple label={"Outils d'intégration"} {...a11yProps(0)} /> */}
					<Tab disableRipple label={"Gestion Cloud"} {...a11yProps(0)} />
					<Tab disableRipple label='Développement Logiciel' {...a11yProps(1)} />
					<Tab disableRipple label='Outils backend' {...a11yProps(2)} />
					<Tab
						disableRipple
						label='Programmation Graphique'
						{...a11yProps(3)}
					/>
					<Tab disableRipple label='Développement FrontEnd' {...a11yProps(4)} />
				</Tabs>
			</Box>

			<Box
				sx={{
					width: "800px",
				}}
				className={`${style.skills_container} ${
					value == 0 && style.skills_container_f
				}`}
			>
				{/* <IntegrationSkills value={value} index={0} /> */}
				<SystemSkills value={value} index={0} />
				<SoftwareSkills value={value} index={1} />
				<BackendSkills value={value} index={2} />
				<GraphicsSkills value={value} index={3} />
				<FrontendSkills value={value} index={4} />
			</Box>
		</Box>
	)
}

export default Competences
