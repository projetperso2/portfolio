import React, { FC } from "react"
import styles from "@styles/components/projectbox.module.css"

interface ProjectBoxProps{
  title: String,
  description: String
}

const ProjectBox: FC<ProjectBoxProps> = 
(
  {title = "Titre",
  description = "Description aléatoire"}
) => {
  return (
    <>
      <div className={styles.box}>
        <span className={styles.title}>{title}</span>
        <div className={styles.box_bottom}>
          <p className={styles.desc}>{description}</p>
        </div>
      </div>
    </>
  )
}

export default ProjectBox