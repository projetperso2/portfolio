import React, { FC } from "react"
import styles from "@styles/components/omnybutton.module.css"
import { IconProp } from "@fortawesome/fontawesome-svg-core"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

interface OmnyButtonProps {
	text: String
  alt: string
	outline?: boolean
  link?: string,
  icon?: IconProp,
  openInNewPage?: boolean
}

const OmnyButton: FC<OmnyButtonProps> = (props) => {
  const css = `${styles.omnybutton} ${!props.outline ? styles.default : styles.outline}`
  const iconSvg = props.icon && <FontAwesomeIcon className={styles.icon} icon={props.icon}></FontAwesomeIcon>
  if(props.link){
    return (
      <>
        <a
          className={css}
          href={props.link}
          rel="noreferrer noopener"
          target={props.openInNewPage ? "_blank" : ""}
          title={props.alt}
        >
          {props.text}
          {iconSvg}
        </a>
      </>
    )
  }else{
    return (
      <>
        <button
          className={css}
        >
          {props.text}
          {iconSvg}
        </button>
      </>
    )
  }
}

OmnyButton.defaultProps = {
	text: "Boutton",
  alt: "Un boutton",
	outline: false,
  link: undefined,
  icon: undefined,
  openInNewPage: true,
}

export default OmnyButton
