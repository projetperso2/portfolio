import { IconProp } from "@fortawesome/fontawesome-svg-core"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Link from "next/link"
import React, { FC } from "react"
import style from "@styles/components/omnysociallinks.module.css"

interface OmnySocialLinkProps {
	brandType: IconProp
	link: string
	alt: string
	noColor?: boolean
}

const OmnySocialLink: FC<OmnySocialLinkProps> = (props) => {
	let styleC = {}
	if (props.noColor) {
		styleC = {
			color: "#4a8cfc",
			fontSize: "24px",
			marginLeft: "15px",
			marginRigh: "15px",
		}
	}
	return (
		<>
			<Link href={props.link}>
				<a
					className={props.noColor ? "" : style.sociallink}
					target='_blank'
					title={props.alt}
					rel={"noopener"}
					style={styleC}
				>
					<FontAwesomeIcon icon={props.brandType}></FontAwesomeIcon>
				</a>
			</Link>
		</>
	)
}

OmnySocialLink.defaultProps = {
	noColor: false,
}

export default OmnySocialLink
