import { Grid } from "@mui/material"
import { FC } from "react"
import SkillsPanel, { SkillsPanelProps } from "./Base"
import SkillDisplay from "./SkillDisplay"

import UNIX from "@assets/brand/Tux.svg.png"
import Ansible from "@assets/brand/ansible.png"
import Bash from "@assets/brand/bash-1.svg"
import Docker from "@assets/brand/docker.svg"
import Harbor from "@assets/brand/harbor-icon-color.png"
import Jenkins from "@assets/brand/jenkins.png"
import LXC from "@assets/brand/lxc1.png"
import Podman from "@assets/brand/podman-logo-source.svg"
import Python from "@assets/brand/python-5.svg"

const SystemSkills: FC<SkillsPanelProps> = (props) => {
	return (
		<SkillsPanel {...props}>
			<Grid container>
				<SkillDisplay img={Docker} name={"Docker"} />
				<SkillDisplay img={Jenkins} name={"Jenkins"} />
				<SkillDisplay img={UNIX} name={"GNU / LINUX"} />
				<SkillDisplay img={Harbor} name={"Harbor"} />
				<SkillDisplay img={Ansible} name={"Ansible"} />
				<SkillDisplay img={Python} name={"Python"} />
				<SkillDisplay img={Bash} name={"Bash"} />
				<SkillDisplay img={Podman} name={"Podman"} />
				<SkillDisplay img={LXC} name={"LXC"} />
			</Grid>
		</SkillsPanel>
	)
}

export default SystemSkills
