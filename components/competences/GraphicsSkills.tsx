import { Grid } from "@mui/material"
import { FC } from "react"
import SkillsPanel, { SkillsPanelProps } from "./Base"
import SkillDisplay from "./SkillDisplay"

import GLSL from "@assets/brand/glsl.png"
import LWJGL from "@assets/brand/LWJGL.svg"
import CUDA from "@assets/brand/Nvidia_CUDA_Logo.jpg"
import OpenCL from "@assets/brand/OpenCL_logo.svg.png"
import OpenGL from "@assets/brand/opengl_logo.jpg"
import ThreeJS from "@assets/brand/threejs-1.svg"

const GraphicsSkills: FC<SkillsPanelProps> = (props) => {
	return (
		<SkillsPanel {...props}>
			<Grid container spacing={1}>
				{/* <SkillDisplay img={JavaFX} name={"JavaFX"} />
				<SkillDisplay img={ElectronJS} name={"ElectronJS"} /> */}
				<SkillDisplay img={OpenGL} name={"OpenGL"} />
				<SkillDisplay img={GLSL} name={"GLSL"} />
				<SkillDisplay img={LWJGL} name={"LWJGL"} />
				<SkillDisplay img={CUDA} name={"CUDA"} />
				<SkillDisplay img={OpenCL} name={"OpenCL"} />
				<SkillDisplay img={ThreeJS} name={"ThreeJS"} />
				{/* <Grid item>JavaFX</Grid>
				<Grid item>ElectronJS</Grid>
				<Grid item>OpenGL</Grid>
				<Grid item>GLSL</Grid>
				<Grid item>LWJGL</Grid>
				<Grid item>CUDA</Grid>
				<Grid item>OpenCL</Grid>
				<Grid item>ThreeJS</Grid> */}
			</Grid>
		</SkillsPanel>
	)
}

export default GraphicsSkills
