import { Grid } from "@mui/material"
import { FC } from "react"
import SkillsPanel, { SkillsPanelProps } from "./Base"

import Javascript from "@assets/brand/logo-javascript.svg"
import Typescript from "@assets/brand/typescript.svg"
import SaSS from "@assets/brand/sass-1.svg"
import React from "@assets/brand/react-2.svg"
import Vue from "@assets/brand/vue-js-1.svg"
import SkillDisplay from "./SkillDisplay"

const FrontendSkills: FC<SkillsPanelProps> = (props) => {
	return (
		<SkillsPanel {...props}>
			<Grid container spacing={1}>
				<SkillDisplay img={Javascript} name={"Javascript"} />
				<SkillDisplay img={Typescript} name={"Typescript"} />
				<SkillDisplay img={SaSS} name={"SaSS"} />
				<SkillDisplay img={React} name={"React"} />
				<SkillDisplay img={Vue} name={"Vue"} />
			</Grid>
		</SkillsPanel>
	)
}

export default FrontendSkills
