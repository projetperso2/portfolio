import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"
import { FC } from "react"
interface SkillsPanelProps {
	children?: React.ReactNode
	index: number
	value: number
}

const SkillsPanel: FC<SkillsPanelProps> = (props: SkillsPanelProps) => {
	const { children, value, index, ...other } = props

	return (
		<div
			role='tabpanel'
			hidden={value !== index}
			id={`vertical-tabpanel-${index}`}
			aria-labelledby={`vertical-tab-${index}`}
			{...other}
		>
			{value === index && (
				<Box sx={{ p: 3 }}>
					<Typography>{children}</Typography>
				</Box>
			)}
		</div>
	)
}

export default SkillsPanel
export type { SkillsPanelProps }
