import { Grid } from "@mui/material"
import { FC } from "react"
import SkillsPanel, { SkillsPanelProps } from "./Base"

import BunJS from "@assets/brand/bunjs.png"
import ExpressJS from "@assets/brand/express-109.svg"
import NextJS from "@assets/brand/nextjs-2.svg"
import NodeJS from "@assets/brand/nodejs.svg"
import NuxtJS from "@assets/brand/nuxt-2.svg"
import RabbitMQ from "@assets/brand/rabbitmq.svg"
import Redis from "@assets/brand/redis.svg"
import SocketIO from "@assets/brand/socket-io.svg"
import Springboot from "@assets/brand/spring-3.svg"
import MySQL from "@assets/brand/mysql-6.svg"
import PostgresSQL from "@assets/brand/postgresql.svg"
import Flask from "@assets/brand/flask.svg"
import Django from "@assets/brand/django.svg"
import SkillDisplay from "./SkillDisplay"

const BackendSkills: FC<SkillsPanelProps> = (props) => {
	return (
		<SkillsPanel {...props}>
			<Grid container spacing={1}>
				<SkillDisplay img={NodeJS} name={"NodeJS"} />
				<SkillDisplay img={BunJS} name={"BunJS"} />
				<SkillDisplay img={ExpressJS} name={"ExpressJS"} />
				<SkillDisplay img={NextJS} name={"NextJS"} />
				<SkillDisplay img={RabbitMQ} name={"Rabbit MQ"} />
				<SkillDisplay img={SocketIO} name={"SocketIO"} />
				<SkillDisplay img={Redis} name={"Redis"} />
				<SkillDisplay img={Springboot} name={"Springboot"} />
				<SkillDisplay img={PostgresSQL} name={"Postgres SQL"} />
				<SkillDisplay img={MySQL} name={"MySQL"} />
				<SkillDisplay img={Flask} name={"Flask"} />
			</Grid>
		</SkillsPanel>
	)
}

export default BackendSkills
