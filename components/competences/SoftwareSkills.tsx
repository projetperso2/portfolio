import { Grid } from "@mui/material"
import { FC } from "react"
import SkillsPanel, { SkillsPanelProps } from "./Base"

/* Images */
import Android from "@assets/brand/android-4.svg"
import C from "@assets/brand/c-1.svg"
import Cpp from "@assets/brand/c.svg"
import Dart from "@assets/brand/dart.svg"
import Flutter from "@assets/brand/flutter.svg"
import Gradle from "@assets/brand/gradle-1.svg"
import Java from "@assets/brand/java-4.svg"
import Maven from "@assets/brand/maven.svg.png"
import Python from "@assets/brand/python-5.svg"
import SkillDisplay from "./SkillDisplay"

const SoftwareSkills: FC<SkillsPanelProps> = (props) => {
	return (
		<SkillsPanel {...props}>
			<Grid container spacing={1}>
				<SkillDisplay img={Java} name={"Java"} />
				<SkillDisplay img={C} name={"C"} />
				<SkillDisplay img={Cpp} name={"C++"} />
				<SkillDisplay img={Python} name={"Python"} />
				<SkillDisplay img={Dart} name={"Dart"} />
				<SkillDisplay img={Flutter} name={"Flutter"} />
				<SkillDisplay img={Android} name={"Android"} />
				<SkillDisplay img={Maven} name={"Maven"} />
				<SkillDisplay img={Gradle} name={"Gradle"} />
			</Grid>
		</SkillsPanel>
	)
}

export default SoftwareSkills
