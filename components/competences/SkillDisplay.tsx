import style from "@styles/components/skilldisplay.module.css"
import { FC } from "react"
import Image, { StaticImageData } from "next/image"
import Grid from "@mui/material/Grid"

interface SkillDisplayProps {
	img: StaticImageData
	name: string
}

const SkillDisplay: FC<SkillDisplayProps> = (props) => {
	return (
		<Grid item md={3}>
			<div className={style.container}>
				<div
					style={{
						display: "flex",
						justifyContent: "center",
					}}
				>
					<div
						style={{ width: "100px", height: "100px", position: "relative" }}
					>
						<Image
							className={style.img}
							src={props.img}
							layout='fill'
							objectFit='contain'
						/>
					</div>
				</div>
				<span className={style.name}>{props.name}</span>
			</div>
		</Grid>
	)
}

export default SkillDisplay
