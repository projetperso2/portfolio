import { Grid } from "@mui/material"
import { FC } from "react"
import SkillsPanel, { SkillsPanelProps } from "./Base"
import SkillDisplay from "./SkillDisplay"

import Discord from "@assets/brand/discord.svg"
import Jenkins from "@assets/brand/jenkins.svg"
import SonarQube from "@assets/brand/sonarqube.svg"
import TravisCi from "@assets/brand/travis-ci.svg"

const IntegrationSkills: FC<SkillsPanelProps> = (props) => {
	return (
		<SkillsPanel {...props}>
			<Grid container>
				<SkillDisplay img={Discord} name={"Discord Webhook"} />
				<SkillDisplay img={Jenkins} name={"Jenkins"} />
				<SkillDisplay img={SonarQube} name={"SonarQube"} />
				<SkillDisplay img={TravisCi} name={"TravisCI"} />
			</Grid>
		</SkillsPanel>
	)
}

export default IntegrationSkills
