import React, {CSSProperties, FC} from "react";
import * as CSS from 'csstype';

interface OmnySeparatorProps{
  width?: CSS.Property.Height
  color?: CSS.Property.BackgroundColor
  margin?: CSS.Property.Margin
}

const OmnySeparator: FC<OmnySeparatorProps> = props => {
  const cssProps: CSSProperties = {
    height: props.width,
    backgroundColor: props.color,
    marginTop: props.margin,
    marginBottom: props.margin,
    borderRadius: "25 px"
  }
  return (
    <>
      <div style={cssProps}></div>
    </>
  )
}

OmnySeparator.defaultProps = {
  width: "2px",
  color: "#4a8cfc",
  margin: "3px"
}

export default OmnySeparator