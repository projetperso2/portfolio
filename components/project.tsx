import React, { FC } from "react"
import Image, { StaticImageData } from "next/image"
import styles from "@styles/components/omnyproject.module.css"

interface OmnyProjectProps {
	image?: string | StaticImageData
}

const OmnyProject: FC<OmnyProjectProps> = (props) => {
	return (
		<>
			<div className={styles.project_container}>
				<div className={styles.image_container}>
					{props.image && (
						<Image
							alt={"suu"}
							src={props.image}
							width={100}
							height={100}
							objectFit='contain'
						></Image>
					)}
				</div>
				<div className={styles.project_content}>{props.children}</div>
			</div>
		</>
	)
}

export default OmnyProject
